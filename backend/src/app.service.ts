import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): Object {
    return {
      res: 200,
      message: 'Backend Work',
    };
  }
}
