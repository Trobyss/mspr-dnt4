import { Test, TestingModule } from '@nestjs/testing';
import { UploadController } from './upload.controller';
// @ts-ignore
import { UploadService } from './upload.service';

describe('UploadController', () => {
  let controller: UploadController;
  let service: UploadService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UploadController],
      providers: [UploadService],
    }).compile();

    controller = module.get<UploadController>(UploadController);
    service = module.get<UploadService>(UploadService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('upload service', () => {
    const response = service.CSVToArray('test,test,test', ',');
    console.log(response);
    expect(response).toEqual([['test', 'test', 'test']]);
  });
});
