# Stack

- Backend:
  - Nest Js
  - Typescript
- Frontend:
  - React
  - Typescript
- Database:
  - MySQl

# Launch

```bash
docker-compose -f infra/docker/docker-compose-dev.yml --env-file .env.dev up
```

# Port

Frontend: 8080
API: 3000
