import { useState } from "react";

export function useUpload() {
  const [res, setRes] = useState<Partial<any>>();

  const handleUpload = async (file: any) => {
    console.log(file);
    const data = new FormData();
    data.append("file", file);

    const res = await fetch("http://localhost:3000/upload/file", {
      method: "POST",
      body: data,
    });
    const jsonResponse = await res.json();
    setRes(jsonResponse);
  };

  return { handleUpload, res };
}
