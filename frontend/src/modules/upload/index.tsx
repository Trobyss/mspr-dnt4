import { useCallback, useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import styled from "styled-components";
import { FolderIcon, FileIcon } from "../ui";
import { useUpload } from "./hooks/useUpload";

export function UploadDropZone() {
  const [hasRejected, setHasRejected] = useState(false);
  const [file, setFile] = useState();
  const { handleUpload, res } = useUpload();

  const onDrop = useCallback((acceptedFiles, rejectedFiles) => {
    if (rejectedFiles.length > 0) {
      setHasRejected(true);
      return;
    } else {
      setHasRejected(false);
    }
    setFile(acceptedFiles[0]);
  }, []);

  const onSubmit = () => {
    if (file) {
      handleUpload(file);
    }
  };
  useEffect(() => {
    console.log(res);
  }, [res]);

  const {
    acceptedFiles,
    getRootProps,
    getInputProps,
    isDragActive,
  } = useDropzone({
    onDrop,
    accept: ".csv",
  });

  return (
    <>
      <BorderDropZone hasRejected={hasRejected} {...getRootProps()}>
        <input {...getInputProps()} />
        <FolderIcon height={50} width="auto" />
        {hasRejected ? (
          <ParagraphUpload>
            Le fichier ne semble pas être valide ...
          </ParagraphUpload>
        ) : isDragActive ? (
          <ParagraphUpload>Juste ici ...</ParagraphUpload>
        ) : (
          <ParagraphUpload>Drag & Drop votre fichier ici !</ParagraphUpload>
        )}
      </BorderDropZone>
      <FileList>
        {acceptedFiles.map((file) => (
          <File key={file.name}>
            <FileIcon height={50} width={100} />
            {file.name}
          </File>
        ))}
        {acceptedFiles.length > 0 && (
          <UploadButton onClick={onSubmit} type="button">
            Télécharger
          </UploadButton>
        )}
      </FileList>
    </>
  );
}

const BorderDropZone = styled.div<{ hasRejected: boolean }>`
  border: 2px solid
    ${(props) => (props.hasRejected ? "red" : "rgb(216, 230, 253)")};
  border-style: dashed;
  border-radius: 10px;
  background-color: rgb(251, 252, 254);
  min-height: 200px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  transition: 0.25s ease;
`;

const ParagraphUpload = styled.p`
  color: #adabb0;
`;

const FileList = styled.div`
  display: flex;
  margin-top: 15px;
  flex-direction: column;
`;

const File = styled.p`
  color: #adabb0;
  display: flex;
  margin-top: 15px;
  flex-direction: row;
  align-items: center;
`;

const UploadButton = styled.button`
  background: rgb(251, 252, 254);
  border: 1px solid rgb(216, 230, 253);
  color: black;
  border-radius: 10px;
  padding: 15px 30px;

  cursor: pointer;
  transition: 0.25s ease;
  &:hover {
    background: rgb(92, 148, 247);
    color: white;
  }
`;
