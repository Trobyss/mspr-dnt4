import { BrowserRouter, Route, Switch } from "react-router-dom";

import { Home } from "../../pages";

export enum Routes {
  HOME = "/",
}

export const AppRouter = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path={Routes.HOME} component={Home} />
    </Switch>
  </BrowserRouter>
);
