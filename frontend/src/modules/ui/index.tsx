import styled, { createGlobalStyle } from "styled-components";

export const Container = styled.div`
  height: 100%;
  width: 100%;
`;

export const H1 = styled.h1``;

export const FlexContainer = styled.div<{}>`
  display: flex;
`;

export const GlobalStyle = createGlobalStyle`
  body{
    margin: 0;
    font-family: Arial, Helvetica, sans-serif; 
    background-color: #E7EFFB;
  }
`;

export { ReactComponent as FileIcon } from "./images/file.svg";
export { ReactComponent as FolderIcon } from "./images/folder.svg";
