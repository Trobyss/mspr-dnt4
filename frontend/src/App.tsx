import { AppRouter } from "./modules/navigation/Routes";
import { Container } from "./modules/ui";

function App() {
  return (
    <Container>
      <AppRouter />
    </Container>
  );
}

export default App;
