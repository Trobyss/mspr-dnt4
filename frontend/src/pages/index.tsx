import React from "react";
import styled from "styled-components";
import { FlexContainer } from "../modules/ui";
import { UploadDropZone } from "../modules/upload";

export const Home = () => (
  <CustomCenterContainer>
    <Modal>
      <ModalTitle>Télécharger votre fichier</ModalTitle>
      <ModalSubtitle>Le fichier doit être au format .csv</ModalSubtitle>
      <UploadDropZone />
    </Modal>
  </CustomCenterContainer>
);

const CustomCenterContainer = styled(FlexContainer)`
  display: flex;
  height: 100vh;
  justify-content: center;
  align-items: center;
`;
const Modal = styled.div`
  background-color: white;
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  padding: 30px 15px;
`;
const ModalTitle = styled.h4`
  color: black;
  text-align: center;
  margin: 5px 0px;
`;
const ModalSubtitle = styled.p`
  color: #adabb0;
  text-align: center;
`;
